FROM golang:latest

RUN mkdir /app
COPY . /app
WORKDIR /app/cmd/web

ENV CGO_ENABLED=0
RUN go build -o snippet .

FROM alpine:latest
RUN apk --no-cache add ca-certificates
WORKDIR ./
COPY --from=0 /app /app


CMD ["/app/cmd/web/snippet"]